<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'API\UserController@register');
Route::post('login', 'API\UserController@login');

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('logout', 'API\UserController@logout');
    Route::get('coordinates', 'API\CoordinatesController@index');
    Route::post('coordinates', 'API\CoordinatesController@create');
});
