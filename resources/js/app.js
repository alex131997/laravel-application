import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './views/App'
import Login from './views/Login'
import Register from './views/Register'
import Home from './views/Home'
import addFigure from './views/addFigure'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

axios.defaults.baseURL = 'http://laravel-application/api';

const router = new VueRouter({
    mode: 'history',
    routes: [{
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: true
        }
    },{
        path: '/add-figure',
        name: 'addFigure',
        component: addFigure,
        meta: {
            auth: true
        }
    }, {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    }, {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    }]
});

Vue.router = router;
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    registerData: {url: 'register', method: 'POST', redirect: '/login'},
    loginData: {url: 'login', method: 'POST', redirect: '/'},
    logoutData: {url: 'logout', method: 'POST',  redirect: '/login'},
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
});