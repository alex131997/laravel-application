# Laravel application #
```
PHP 7.2
MySql 5.7
Apache
```

```
create database
```

```
rename .env.example to .env
change in .env file
APP_URL
DB_HOST
DB_PORT
DB_DATABASE
DB_USERNAME
DB_PASSWORD

```

```
In file resources/js/app.js
Change to your URL
axios.defaults.baseURL = 'http://laravel-application/api';

```

```
#!bash

composer install
```

```
#!bash

php artisan key:generate
```

```
#!bash

php artisan storage:link
```

```
#!bash

php artisan migrate --seed
```

```
#!bash

php artisan passport:install
```

```
#!bash

npm install
```


```
#!bash

npm run production
```