<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    private const DIAMETER_EARTH = 12742; // 2 * R; R = 6371 km
    private const PI = 0.017453292519943295; // PI / 180

    protected $fillable = ['user_id' ,'coordinates', 'perimeter'];

    public function user() {
        return $this->belongsTo(User::class, 'id');
    }

    function distance($lat1, $lon1, $lat2, $lon2) {
        $a = 0.5 - cos(($lat2 - $lat1) * self::PI)/2 +
            cos($lat1 * self::PI) * cos($lat2 * self::PI) *
            (1 - cos(($lon2 - $lon1) * self::PI))/2;

        return self::DIAMETER_EARTH * asin(sqrt($a));
    }
}
