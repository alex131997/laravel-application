<?php

namespace App\Http\Controllers\API;

use App\Coordinate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CoordinatesController extends Controller
{
    public function index(Coordinate $coordinate){
        $coordinates = $coordinate->where('user_id', Auth::user()->id)->get();
        return response()->json($coordinates, 200);
    }

    public function create(Request $request, Coordinate $coordinate){
        $validator = Validator::make($request->all(), [
            'coordinates' => 'required|array',
            '*.*.latitude' => ['required','regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,14})?))$/'],
            '*.*.longitude' => ['required','regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,14})?))$/'],
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $count = count($request->coordinates);
        if($count > 2){
            $perimeter = 0;
            foreach ($request->coordinates as $key => $coord){
                if($key == $count - 1){
                    $secondLatitude = $request->coordinates[0]['latitude'];
                    $secondLongitude = $request->coordinates[0]['longitude'];
                } else{
                    $secondLatitude = $request->coordinates[$key + 1]['latitude'];
                    $secondLongitude = $request->coordinates[$key + 1]['longitude'];
                }
                $perimeter += $coordinate->distance($coord['latitude'], $coord['longitude'], $secondLatitude, $secondLongitude);
            }
            $coordinate->perimeter = $perimeter;
        }
        $coordinate->coordinates = json_encode($request->coordinates);
        $coordinate->user_id = Auth::user()->id;
        $coordinate->save();

        return response()->json(['success' => "Coordinates was saved"], 200);
    }
}